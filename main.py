import time
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')


def fibonacci(n):
    if n == 1:
        return 0
    elif n == 2:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


def function_to_measure():
    fibonacci(30)


def perform_time_measurement(func):
    time1 = time.perf_counter()
    func()
    time2 = time.perf_counter()
    func()
    func()
    time3 = time.perf_counter()
    func()
    func()
    func()
    time4 = time.perf_counter()
    func()
    func()
    func()
    func()
    time5 = time.perf_counter()
    func()
    func()
    func()
    func()
    func()
    time6 = time.perf_counter()
    func()
    func()
    func()
    func()
    func()
    func()
    time7 = time.perf_counter()
    func()
    func()
    func()
    func()
    func()
    func()
    func()
    time8 = time.perf_counter()

    duration1 = time2 - time1
    duration2 = time3 - time2
    duration3 = time4 - time3
    duration4 = time5 - time4
    duration5 = time6 - time5
    duration6 = time7 - time6
    duration7 = time8 - time7
    durations = [duration1, duration2, duration3, duration4, duration5, duration6, duration7]
    # convert to milliseconds:
    durations = [1000*seconds for seconds in durations]

    return durations


myResult2 = perform_time_measurement(function_to_measure)
print("Fitting straight line through measurement points...")
x = [1,2,3,4,5,6,7]
m, b = np.polyfit(x, myResult2, 1)

print("The execution time is %.2f milliseconds" % m)

x = np.array(x)
y = np.array(myResult2)

plt.plot(x, y, 'o')

f = lambda x: m*x + b
plt.plot(x,f(x))

plt.xlabel("Measurement #")
plt.ylabel("Execution Time (ms)")

plt.savefig('plot.png')

print("Saved plot to ./plot.png")
